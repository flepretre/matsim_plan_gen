# plan_gen

Python generator for MATSim random plans.

## setup

use `venv` (https://github.com/juliendehos/venv)

## prerequisites

MATSim network files (`.xml`)
*(test files are available in `input/` folder)*

## install

    venv i plan_gen
    venv a plan_gen
    pip install pkgconfig setuptools
    pip install . --upgrade

## usage

### run

    plan_gen_cli.py [ params ] > out_plans.xml

### params list
- `np` : number of persons to generate,
- `nw` : global network file path,
- `nc` : number of clusters by axis *(e.g. `nc=100` results in `100 x 100 = 10000` clusters)*,
- `hc` *(optional)* : home clusters center coordinates (`line:column`). If multiple clusters, separate centers coordinates with `|` symbol (`"line_1:col_1|line_2:col_2|...|line_x:col_x"`),
- `hr` *(only if `hc` parameter is specified)* : home clusters radii. If multiple clusters, separate radii with `|` symbol (`"rad_1|rad_2|...|rad_x"`),
- `wc` *(optional)* : work clusters center coordinates (cf. `hc`),
- `wr` *(only if `wc` parameter is specified)* : work clusters radii (c.f `hr`).

### examples

generate 5000 persons centered in a 100x100 clusters grid (default):

    plan_gen_cli.py np=5000,nw=input/network.xml,nc=100 > out.xml

generate 10000 persons with home locations reparted in 2 clusters of radii 10 and 5, respectively *(work locations are sampled with the default behaviour as above)*:

    plan_gen_cli.py np=10000,nw=input/network.xml,nc=100,\
    hc="50:50|67:17",hr="10|5" > out.xml
