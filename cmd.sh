POP=$1

echo "1_1..."
plan_gen_cli.py nw=input/network.xml,nc=100,np=${POP},hc="50:50",hr="50",hw="1",wc="50:50",wr="20",ww="1" > plans${POP}_1_1.xml

echo "1_4..."
plan_gen_cli.py nw=input/network.xml,nc=100,np=${POP},hc="50:50",hr="50",hw="1",wc="45:30|70:45|28:74|56:80",wr="5|5|5|5",ww="2|1|2|1" > plans${POP}_1_4.xml

echo "4_1..."
plan_gen_cli.py nw=input/network.xml,nc=100,np=${POP},hc="46:36|69:44|50:75|20:75",hr="20|10|20|8",hw="1|1|1|1",wc="50:50",wr="20",ww="1" > plans${POP}_4_1.xml

echo "4_4..."
plan_gen_cli.py nw=input/network.xml,nc=100,np=${POP},hc="46:36|69:44|50:75|20:75",hr="20|10|20|8",hw="1|1|1|1",wc="45:30|70:45|28:74|56:80",wr="5|5|5|5",ww="2|1|2|1" > plans${POP}_4_4.xml

echo "uni_1..."
plan_gen_cli.py nw=input/network.xml,nc=100,np=${POP},hc="50:50",hr="50000",hw="1",wc="50:50",wr="20",ww="1" > plans${POP}_uni_1.xml

echo "uni_4..."
plan_gen_cli.py nw=input/network.xml,nc=100,np=${POP},hc="50:50",hr="50000",hw="1",wc="45:30|70:45|28:74|56:80",wr="5|5|5|5",ww="2|1|2|1" > plans${POP}_uni_4.xml
