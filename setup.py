import pkgconfig
from setuptools import setup

setup(
    name = 'plan_gen',
    version = '0.1',
    scripts = ['plan_gen/plan_gen_cli.py'],
    packages = ['plan_gen'],
    install_requires = [
        'numpy', 
        'lxml',
        'matplotlib',
        'joblib',
        'pandas',
        'utm'
    ],
)

